import Head from 'next/head'
import Image from 'next/image'
import React from 'react'
import styles from '../styles/Home.module.css'
import Link from 'next/link'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Welcome NestJs Cours</title>
        <meta name="description" content="NestJS cours" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Bienvenue au cours Next.js !
        </h1>

        <p className={styles.description}>
            <Link href="/tasks">Liste des tâches</Link>
        </p>

      </main>
      <footer className={styles.footer}>

          Powered by Helyx !

      </footer>
    </div>
  )
}
