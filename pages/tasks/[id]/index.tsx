import {Task} from "../../../dto/tasks.model";


type ITaskProps  = {
    task: Task;
}

export default function TaskScreen(props : ITaskProps) {

    //REACT !!!!

    return (
        <main>
            <table>
                <th>
                    title
                </th>
                <th>
                    description
                </th>
                <th>
                    status
                </th>
                    <tr>
                        <td>{props.task.title}</td>
                        <td>{props.task.description}</td>
                        <td>{props.task.status}</td>
                    </tr>
            </table>

        </main>
    )

}

export async function getServerSideProps(context: any) {

        const res = await fetch(`http://localhost:3001/tasks/${context.query.id}`)
    const task : Task = await res.json()
    if (!task) {
        return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        }
    }
    return {
        props: { task }, // will be passed to the page component as props
    }

}