import {useRouter} from "next/router";
import {Task} from "../../dto/tasks.model";
import Link from 'next/link'

type ITasksProps  = {
    tasks: Task[];
}

export default function Tasks(props : ITasksProps) {

    //REACT !!!!

    return (
        <main>
            <table>

            { props.tasks.map((task: Task, index : number)=> (
                <tr>
                    <td>{task.title}</td>
                    <td>{task.description}</td>
                    <td><Link href={`tasks/${task.id}`}>Lien</Link></td>
                </tr>
            ))}
            </table>

        </main>
    )

}

export async function getStaticProps(context: any) {

    const res = await fetch(`http://localhost:3001/tasks`)
    const tasks : Task[] = await res.json()
    if (!tasks) {
        return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        }
    }
    return {
        props: { tasks }, // will be passed to the page component as props
        revalidate : 15
    }

}